import numpy as np


def calculate_subgradient(A, b, x_lamb):
    ''' Calcula el subgradiente g_r

        Parametros:
        A (list): vector fila en cuestion restriccion
        b (int): valor de la restriccion
        x_lamb (matrix): matriz de la minimización de la función dual
        
        Devuelve:
        g_r (list): para cada restricción existirá un elemento g_r

    '''
    
    return A @ x_lamb - b

def lagrangian_function(objetive_fun, x, A, b, lambda_mult):
    return float(objetive_fun(x) + lambda_mult * (A*x-b))

def replace_negative_val(actual_lambda):
    return np.asarray([0 if x < 0 else x for x in actual_lambda]) 


def calculate_alpha(y_r, zeta_lambda, f_lambda, g_r):
    '''
    Devuelve el valor de alpha_r

    Parametros:
    zeta_lambda (list): imagen de la función dual
    f_lambda (float): imagen de la función original
    g_r (list): lista de gradientes

    Devuelve:
    alpha_r (float): calcular el paso de alpha mediante los parametros

    '''
    
    norm = np.linalg.norm(g_r)
    # Eq 5.30
    return y_r * (f_lambda - zeta_lambda) / pow(norm,2)



def update_lambda_multiplier(actual_lambda, alpha_r, sub_gradient):
    '''
    Actualiza los multiplicadores lambda

    Parámetros:
    actual_lambda (list): lista de multiplicadores de lambda actualizados
    alpha_r (float): paso alpha_r
    sub_gradient (list): vector de subgradientes

    Devuelve
    new_lambda (list): vector de multiplicadores actualizados

    '''
    # Eq 5.27
    # We will have some lists there, so we have to take the max val in the future multiplier lagrangian list
    new_lambda = []
    gr = np.asarray(sub_gradient).reshape(-1)

    for x in range(0, len(actual_lambda)):
        new_lambda.append(max(0, actual_lambda[x] + alpha_r * gr[x]))
    
    return new_lambda
