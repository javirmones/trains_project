#!/usr/bin/python3.8
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np

MAX_IT = 500

def solve_relaxed_problem(lambda_mult):

    '''
        Resolver el problema relajado en cuestión, existirán un número de dimensiones
        la idea es que para cada restricción existirá un multiplicador de lagrange, por 
        lo que actualmente como solo hay una el vector lambda_mult sera = [lambda]

        Parametros:
        lambda_mult (list): vector de multiplicadores de lagrange
        n (int): dimensión de las restricciones

        Devuelve:
        x_lambda (matrix): vector columna con los elementos minimizados del problema dual

    '''
    vec = []
    for i in range(0, len(lambda_mult)):
        if i == 0:
            vec.append([(lambda_mult[i]+lambda_mult[i+1])/2])
        elif i == 1:
            vec.append([(lambda_mult[i-1]+lambda_mult[i+1])/2])
        elif i == 2:
            vec.append([lambda_mult[i-2]/2])


    #vec.append([(lambda_mult[0]+lambda_mult[1])/2])
    #vec.append([(lambda_mult[0]+lambda_mult[2])/2])

    return np.matrix(vec, dtype=float)



def update_lambda_multiplier(actual_lambda, alpha_r, sub_gradient):
    '''
    Actualiza los multiplicadores lambda

    Parámetros:
    actual_lambda (list): lista de multiplicadores de lambda actualizados
    alpha_r (float): paso alpha_r
    sub_gradient (list): vector de subgradientes

    Devuelve
    new_lambda (list): vector de multiplicadores actualizados

    '''
    # Eq 5.27
    # We will have some lists there, so we have to take the max val in the future multiplier lagrangian list
    new_lambda = []
    gr = np.asarray(sub_gradient).reshape(-1)
    print("gr- update",gr)

    for x in range(0, len(actual_lambda)):
        new_lambda.append(max(0, actual_lambda[x] + alpha_r * gr[x]))
    

    print("prueba",new_lambda)
    return new_lambda




def replace_negative_val(actual_lambda):
    return np.asarray([0 if x < 0 else x for x in actual_lambda]) 
    


def calculate_alpha(zeta_lambda, f_lambda, g_r):
    '''
    Devuelve el valor de alpha_r

    Parametros:
    zeta_lambda (list): imagen de la función dual
    f_lambda (float): imagen de la función original
    g_r (list): lista de gradientes

    Devuelve:
    alpha_r (float): calcular el paso de alpha mediante los parametros

    '''
    y_r = 0.5
    norm = np.linalg.norm(g_r)
    # Eq 5.30
    return y_r * (f_lambda - zeta_lambda) / pow(norm,2)   

def calculate_best_primal_val(x_lambda, previous_primal_val):
    ''' Calcula el mejor valor de la imagen de la funcion

        Parametros:
        x_lambda (matrix): es el vector columna que minimiza la funcion

        previous_primal_val (int): es el valor anterior calculado de V[f(x_p)]

        Devuelve:
        f_lambda si se cumple la restriccion (2) y si el valor actual de f_lambda es mejor que el anterior
        si no devuelve el valor previo
        (A * x_lambda <= b).all()
    '''
    #1 Put the values in x_lambda to sum that
    f_xlambda = float(f(x_lambda))
    #2 Return the value if satisfy the restriction
    return f_xlambda if sum(x_lambda) >= 1 and x_lambda[0] >= 0.4 and x_lambda[1] >= 0.1 and f_xlambda < previous_primal_val else previous_primal_val
    
def f(matrix):
    ''' Devuelve el sumatorio de los elementos de un vector fila/columna

        Parametros:
        matrix (matrix): matriz en cuestion que quiere ser sumada
        
        Devuelve:
        sumatory (int): sumatory de los elementos del vector fila/columna
    '''
    sumatory = 0
    for x in matrix:
        sumatory += pow(x, 2)
    return sumatory


def lagrangian_function(x, A, b, lambda_mult):
    return float(f(x) + lambda_mult * (A*x-b))


def calculate_subgradient(A, b, x_lamb):
    ''' Calcula el subgradiente g_r

        Parametros:
        A (list): vector fila en cuestion restriccion
        b (int): valor de la restriccion
        x_lamb (matrix): matriz de la minimización de la función dual
        
        Devuelve:
        g_r (list): para cada restricción existirá un elemento g_r

    '''
    
    return A @ x_lamb - b
    #print("GR ANTES DEL CALCULO", g_r)
    #return np.asarray(g_r).reshape(-1)


    

if __name__ == "__main__":
    # First init lagrange multiplicators and values
    
    lambda_mult = np.array([50, 50, 50])
    g_r = []

    primal_val = f(np.asarray([50, 50,25]))
    historic_primal_val = []
    historic_dual_val = []
    A = np.asarray([[-1, -1, -1],[-1, 0, 0], [0, -1, 0]])
    b = np.asarray([[-1], [-0.4], [-0.1]])

    for iteracion in range(0, MAX_IT):

        print("----NUEVA ITERACION-----", iteracion)
        print("lambda mult", lambda_mult)
        # 1) Solve Relaxed problem
        x_lambda = solve_relaxed_problem(lambda_mult)
        print("x_lambda", x_lambda)
        # 2) Calc subgradient g_r
        g_r = calculate_subgradient(A, b, x_lambda)
        
        print("Gradiente g_r",g_r)

        # 3 Generate a solution best primal val and dual function
        if iteracion == 0:
            previous_primal_val = primal_val
        else: 
            previous_primal_val = historic_primal_val[iteracion-1]

        print("f lambda previo",previous_primal_val)
        f_lambda = calculate_best_primal_val(x_lambda, previous_primal_val)
        print("f lambda ", f_lambda)
        historic_primal_val.append(f_lambda)
        zeta_lambda = lagrangian_function(x_lambda, A, b, lambda_mult)
        print("zeta lambda", zeta_lambda)
        historic_dual_val.append(zeta_lambda)

        # 4) Calculate stepsize alpha_r
        alpha_r = calculate_alpha(zeta_lambda, f_lambda,  g_r)
        print("alpha_r", alpha_r)

        # 5) Stop cicle
        # 
        if round(zeta_lambda,8) == round(f_lambda,8) :
            break

        # 6) Update lambda multipliers
        lambda_mult = update_lambda_multiplier(lambda_mult, alpha_r, g_r)
        print("new_lambda_mult", lambda_mult)
        print("----FIN ITERACION---",iteracion)
        #salida = int(input("FIN ITERACION -> 0 SALIR 1-> CONTINUAR" ))
        #if salida == 0:
        #    break


        
    print("Historic_primal_val", historic_primal_val)
    print("Historic_dual_val", historic_dual_val)
    print(len(historic_dual_val))
    print(len(historic_primal_val))
    # 7 imprimir

    plt.suptitle("Evolución de gráfica")
    plt.plot(historic_primal_val, label= "V[f(xp)]")
    plt.plot(historic_dual_val, label="Zeta(lambda)")
    plt.legend(loc="upper left")
    plt.xlabel("Iteracciones")
    plt.ylabel("V[f(xp)] / Zeta(lambda)")
    plt.show()
    
    
    

        
        
    


    
