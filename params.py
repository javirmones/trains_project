
from train import Train

# Resistencia tren AVE TGV Sud Est masa en T : 418 r : [235, 3.09, 0.0535]
# Resistencia tren AVANT S448 masa en t : 150.40 r : [188, 1.805, 0.06016]


t1 = Train(index = 1, stat = 1.00, et = 120, vt = [0], 
            ticket = 50, pasajeros = 400, tipos_pasajero = [],
             resistencia_movimiento = [225, 2.710, 0.0450])
             
t2 = Train(index = 2, stat = 0.75, et= 160, vt = [0],
            ticket = 20, pasajeros = 200, tipos_pasajero=[], 
            resistencia_movimiento = [188, 1.805, 0.01830])



common_parameters = {
    "Z" : [0.15, 0.30, 0.45],
    "DelaysAvant" : [50, 100, 100],
    "T" : [t1, t2],
    "sigma" : 15.5,
    "c_delta" : 500,
}
