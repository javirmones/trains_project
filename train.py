#!/usr/bin/python3.8
# -*- coding: utf-8 -*-

class Train:
    
    __tren = None
    __stat = None
    __et = None
    __precio_ticket = None
    __numero_pasajeros = None
    __candidatos = None
    __tipos_pasajeros = None
    __resistencia_movimiento = None
    __vt = None

    def __init__(self, index, stat, et, vt, ticket, pasajeros, tipos_pasajero, resistencia_movimiento):
        self.__tren = index
        self.__stat = stat
        self.__et = et
        self.__precio_ticket = ticket
        self.__numero_pasajeros = pasajeros
        self.__tipos_pasajeros = tipos_pasajero
        self.__vt = vt
        self.__candidatos = []
        self.__resistencia_movimiento = resistencia_movimiento

    def get_stat(self):
        return self.__stat

    def set_stat(self, stat):
        self.__stat = stat

    def get_et(self):
        return self.__et

    def set_et(self, et):
        self.__et = et
    
    def get_ticket_prize(self):
        return self.__precio_ticket

    def set_ticket_prize(self, prize):
        self.__precio_ticket = prize

    def get_numero_pasajeros(self):
        return self.__numero_pasajeros

    def set_numero_pasajeros(self, n):
        self.__numero_pasajeros = n

    def get_tipos_pasajeros(self):
        return self.__tipos_pasajeros
    
    def set_tipos_pasajeros(self, tipos):
        self.__tipos_pasajeros = tipos

    def get_candidatos(self):
        return self.__candidatos

    def set_candidatos(self, candidatos):
        self.__candidatos = candidatos

    def get_name(self):
        return self.__tren

    def get_vt(self):
        return self.__vt
    
    def set_vt(self, vt):
        self.__vt.append(vt)

    def get_resistencia(self):
        return self.__resistencia_movimiento

    def __str__(self):
        return ("Tren: " + str(self.__tren) +
        "\nTiempo estimado de llegada: " 
        + str(self.__stat) + " horas \nDistancia del tren: " 
        + str(self.__et) + " km \nPrecio ticket: " 
        + str(self.__precio_ticket) + " euros \nNumero pasajeros: " 
        + str(self.__numero_pasajeros) + "\nTipos de pasajeros: " 
        + str(self.__tipos_pasajeros) + "\nVelocidad actual: "
        + str(self.__vt) + "\nCandidatos al optimo:"
        + str(self.__candidatos))
    

