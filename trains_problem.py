#!/usr/bin/python3.8
# -*- coding: utf-8 -*-

from lagrangian_relaxation import *
from params import common_parameters as parameters
from graphics import show_graphics
import numpy as np


def tau_f(delay_level, arrival_time):
    # Equation 26
    Z = parameters["Z"]
    return arrival_time + Z[delay_level]


def k_t(w_t):
    # Equation 27
    # k_t = c_d + w_t / rho
    return (parameters["c_delta"] + w_t) / parameters["sigma"]

def v_t(e_t, arrival_time):
    # Equation v = space / time
    return e_t / arrival_time

def calculate_energy(e_t, v_t, resistance):
    # Equation 41
    # UB = rho * sum tET E(tau_ft) = rho * sum tET et(r0 + r1*vt + r2+vt^2)
    r0, r1, r2 = resistance
    return e_t * (r0 + r1 * v_t + r2 * pow(v_t, 2))


def convert_lambda_multipliers(lambda_mult):
    # Equation 23
    # Cambiar los multiplicadores lambda
    w = []
    for x in range(0, len(lambda_mult)):
        if x == 0:
            w.append(lambda_mult[x])
        elif x > 0:
            w.append(lambda_mult[x] - lambda_mult[x-1])

            
    w.append(-lambda_mult[len(lambda_mult)-1])

    return np.asarray(w)


def X_dseta(delta, delay_delta):
    # Función indicadora 
    # 1 si delta esta entre los rangos determinados 0 si no
    
    Z = parameters["Z"]
    if delay_delta == len(Z) - 1:
        return 1 if delta > Z[delay_delta] else 0

    return 1 if delta in np.arange(Z[delay_delta], Z[delay_delta+1], 0.000001) else 0

def new_X_dseta(delta, delay_delta):
    Z = parameters["Z"]
    if delay_delta == len(Z) - 1:
        return 0
    
    return 1 if delta <= Z[delay_delta+1] and delta >= Z[delay_delta] else 0 


def A_dseta(z, train):
    # At = sum vEY dv * ct,v * phi
    compensation = parameters["DelaysAvant"]
    return train.get_ticket_prize() * train.get_numero_pasajeros() * compensation[z] / 100


def objetive_function(delta):
    # Funcion objetivo

    T = parameters["T"]
    result_compensation = 0
    result_energy_consumption = 0
    
    for t in range(0, len(T)):
       result_compensation += C(delta[t], T[t])
    # SUMAR TODOS LOS Lt

    for t in range(0, len(T)):
        result_energy_consumption += calculate_energy(T[t].get_et(), T[t].get_vt(), T[t].get_resistencia())

    return result_compensation + result_energy_consumption


def C(delay_delta, train):
    # Costes C(deltat) = sum dsetaEZ At * Xs(deltat) + cd *delta
    Z = parameters["Z"]
    delay_costs = 0

    for z in range(0, len(Z)):
        delay_costs += A_dseta(z, train) * new_X_dseta(delay_delta, z) + parameters["c_delta"] * delay_delta    


    return delay_costs


def calculate_vp(k, resistance):
    _, r1, r2 = resistance
    # Equation 39
    # Candidato a tauft 
    coeff = [2*r2, r1, 0, -k]
    raices =  np.roots(coeff)
    vp = [float(x) for x in raices if x > 0] 
    return vp.pop()
    

def optimization_problem(tau_v ,wt, t):
    opt_result = 0
    tau_img = []

    for tf in tau_v:
        
        for delay_level in range(0, len(parameters["Z"])):
            opt_result += A_dseta(delay_level, t) * X_dseta(tf- t.get_stat(), delay_level)

        tau_img.append(parameters["c_delta"] * t.get_stat() + opt_result 
                   + calculate_energy(t.get_et(), t.get_vt()[-1], t.get_resistencia())
                   + (parameters["c_delta"] + wt) * tf )
    
    return min(tau_img)

def stop_cicle(sub_gradient):
    vector = np.asarray(sub_gradient).reshape(-1)
    cont = 0
    
    for x in range(0, len(vector)):
        if vector[x] == 0:
            cont += 1
    
    return 1 if cont == len(vector) else 0 



def start():

    MAX_IT = 5
    # Iniciar gradiente
    g_r = []
    T  = parameters["T"]
    historic_ub = []
    historic_lb = []
    historic_lu = []
    iteracion = 0
    ub = 0
    lb = 0
    # Restricciones
    # Se inician tres trenes, dos restricciones tau1 - tau 2 <= 0 y tau2 - tau3 <= 0
    # Podriamos automatizar este step
    lambda_mult = np.array([0])
    A = np.asarray([[1, -1]])
    b = np.asarray([[0]])
    
    
    for t in T:
        # Calcular la velocidad para tau_f = stat
        vt = v_t(t.get_et(), t.get_stat())
        t.set_vt(vt)
        
        # Calcular la energia
        ub += calculate_energy(t.get_et(), vt, t.get_resistencia())
        
    historic_ub.append(parameters["sigma"] * ub)
    print("Limite_superior_energia ", historic_ub)
    

    for iteracion in range(0, MAX_IT):
        print("Lambda mult", lambda_mult)
        wt = convert_lambda_multipliers(lambda_mult)
        print("wt ", wt)
        kt = k_t(wt)
        print("kt ", kt)
        tau_f_opt = []


        for t in range(0, len(T)):

            candidatos_tau = []
            

            for l in range(0, len(parameters["Z"])):

                candidatos_tau.append(tau_f(l, T[t].get_stat()))
            
            velocidad = calculate_vp(kt[t], T[t].get_resistencia())
            T[t].set_vt(velocidad)

            candidatos_tau.append(T[t].get_et()/velocidad)


            T[t].set_candidatos(candidatos_tau)
            
            # Esta función recibirá los candidatos a óptimo y
            #  devolverá el tau óptimo comprobando la imagen mínima, eso será Lt(um)
            tau_f_opt.append([optimization_problem(candidatos_tau, wt[t], T[t])])


            # Despues sumamos todos los LT(um)
        print("Imagenes minimas de cada tren", tau_f_opt)
        x_lambda = np.matrix(tau_f_opt, dtype=float)
        print("xlambda", x_lambda)
        historic_lu.append(sum(tau_f_opt[x][0] for x in range(0,len(tau_f_opt))))

        print("L(u"+str(iteracion)+")",historic_lu)

        
        # Calcular la funcion objetivo para el límite inferior con valores muy bajos
        # objetive function
        # actualizar el lower bound, el maximo entre el L(um) y el Lower bound
        if iteracion == 0:
            lb = calculate_energy(50, 50, parameters["T"][0].get_resistencia())
            historic_lb.append(lb)
            print("primer lowerbound", lb)
        else:
          

            lb = max(historic_lb[iteracion-1], historic_lu[iteracion])
            historic_lb.append(lb)
            print("historic lb",historic_lb)
            print("historico de lu", historic_lu)
            
 
        # Calcular subgradiente, con A * x_lambda - b, siendo x_labmda el tau optimo
        g_r = calculate_subgradient(A, b, x_lambda)
        print("g_"+str(iteracion)+" = ",g_r)
        # Criterio de parada, si el gradiente es igual a 0 en todas sus componentes se detiene y L(um),
        #ses la solución optima
  
        alpha_r = calculate_alpha(y_r=0.1, zeta_lambda = ub, f_lambda = lb, g_r= g_r)
        # Actualizar los multiplicadores, con la formula
        lambda_mult = update_lambda_multiplier(lambda_mult, alpha_r, g_r)
    
    for t in T:
        print(t)

    #show_graphics(historic_ub,historic_lb,"LT","LB" )
    #show_graphics(historic_lb,historic_ub,"LB","UB" )
        
    return []
        

        
        
        





    
   