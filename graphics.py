#Aqui pretendo meter metodos para poder ver la evolución de las graficas con los trenes
import matplotlib.pyplot as plt

def show_graphics(value_x, value_y, label_x, label_y):
    plt.suptitle("Evolución de gráfica")
    plt.plot(value_x, label=label_x)
    plt.plot(value_y, label=label_y)
    plt.legend(loc="upper left")
    plt.xlabel(label_x)
    plt.ylabel(label_y)
    plt.show()
    